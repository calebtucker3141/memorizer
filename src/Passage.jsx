import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import Slider from 'material-ui/Slider';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import db from './db';
import Words from './Words';

function parseWords(text, percent) {
  const words = [];
  // split(/[\s,]+/)
  let i = 0;

  const massagedText = text.replace(/-/, '- ');

  massagedText.split(/[\s]+/).forEach((val) => {
    let word = val;
    let punctuation = null;

    [',', '.', '!', '?', '-'].forEach((char) => {
      if (val.endsWith(char)) {
        punctuation = char;
        word = word.substring(0, word.length - 1);
      }
    });

    words.push({
      key: i += 1,
      value: word,
      show: Math.random() > percent,
      punctuation: false,
    });

    if (punctuation !== null) {
      words.push({
        key: i += 1,
        value: punctuation,
        show: true,
        punctuation: true,
      });
    }
  });
  return words;
}



class Passage extends Component {

  constructor(props) {
    super(props);
    const passage = db.get(props.match.params.id);
    this.state = {
      passage: passage.text,
      words: parseWords(passage.text, 0.5),
      percent: 0.5,
      reveal: false,
      edit: false,
    };
    this.flip = this.flip.bind(this);
    this.reveal = this.reveal.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.delete = this.delete.bind(this);
    this.handlePercent = this.handlePercent.bind(this);
  }

  handleEdit() {
    this.setState({ edit: !this.state.edit });
  }

  flip(sender) {
    if (!sender.punctuation) {
      sender.show = !sender.show;
      this.setState({ words: this.state.words });
    }
  }

  reveal() {
    this.setState({ reveal: !this.state.reveal });
  }

  delete() {
    db.delete(this.props.match.params.id);
    this.context.router.history.push('/');
  }

  handlePercent(event, value) {
    this.setState({ words: parseWords(this.state.passage, 1.0 - value), percent: value });
  }

  render() {
    return (
      <div>
        <AppBar
          title="Memorizer"
          iconElementLeft={
            <IconButton containerElement={<Link to="/" />}><ArrowBack /></IconButton>
          }
          iconElementRight={<FlatButton label={this.state.edit ? 'Done' : 'Edit'} onClick={this.handleEdit} />}
        />

        <div id="words">
          <Words {...this.state} onFlip={this.flip} />
        </div>

        { this.state.edit &&
          <div id="slider-container">
            <Slider value={this.state.percent} onChange={this.handlePercent} />
          </div>
        }

        <div id="reveal-container">
          <RaisedButton label="Reveal" default fullWidth onTouchTap={this.reveal} style={{ height: '75px' }} labelStyle={{ lineHeight: '75px' }} />
        </div>

        { this.state.edit &&
          <div id="delete-container">
            <RaisedButton label="Delete" secondary fullWidth onTouchTap={this.delete} />
          </div>
        }

      </div>
    );
  }
}

Passage.contextTypes = {
  router: PropTypes.object,
};

Passage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object.isRequired,
  }).isRequired,
};

export default Passage;
