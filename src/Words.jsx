import React from 'react';
import PropTypes from 'prop-types';
import Word from './Word';

const Words = ({ words, reveal, onFlip }) => (
  <div>
    { words.map(item => <Word key={item.key} reveal={reveal} item={item} onFlip={onFlip} />)}
  </div>
);

Words.propTypes = {
  words: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.number,
  })).isRequired,
  reveal: PropTypes.bool.isRequired,
  onFlip: PropTypes.func.isRequired,
};

export default Words;
