import shortid from 'shortid';
import store from 'store2';

class DB {

  static list() {
    return store.get('passages') || [];
  }

  static add(passage) {
    const items = this.list();
    const id = shortid.generate();
    items.push({ id, text: passage });
    store.set('passages', items);
    return id;
  }

  static get(passageId) {
    const items = this.list();
    return items.find(item => item.id === passageId);
  }

  static delete(passageId) {
    const items = this.list().filter(item => item.id !== passageId);
    store.set('passages', items);
  }
}

export default DB;
