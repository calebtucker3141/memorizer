import React, { Component } from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import myTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {
  HashRouter as Router,
  Route,
} from 'react-router-dom';
import './App.css';
import Add from './Add';
import Passage from './Passage';
import List from './List';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

class App extends Component {
  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(myTheme)}>
        <Router>
          <div>
            <Route exact path="/" component={List} />
            <Route path="/passages/:id" component={Passage} />
            <Route path="/add" component={Add} />
          </div>
        </Router>
      </MuiThemeProvider>
    );
  }
}

export default App;
