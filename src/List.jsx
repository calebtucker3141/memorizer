import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import { Link } from 'react-router-dom';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import PropTypes from 'prop-types';
import db from './db';

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      passages: db.list(),
      passageId: null,
    };
    this.handleRowSelection = this.handleRowSelection.bind(this);
  }

  handleRowSelection(rowNumber) {
    const passageId = this.state.passages[rowNumber].id;
    this.context.router.history.push(`/passages/${passageId}`);
  }

  render() {
    return (
      <div>
        <AppBar
          title="Memorizer"
          iconElementRight={<FlatButton label="Add New" containerElement={<Link to="/add" />} />}
        />
        <Table onRowSelection={this.handleRowSelection}>
          <TableBody displayRowCheckbox={false}>
            {this.state.passages.map((item) => {
              return (
                <TableRow key={item.id}>
                  <TableRowColumn>{item.text}</TableRowColumn>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </div>
    );
  }
}

List.contextTypes = {
  router: PropTypes.object,
};

export default List;
