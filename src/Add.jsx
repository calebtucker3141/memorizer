import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import db from './db';

class Add extends Component {

  constructor(props) {
    super(props);
    this.state = { };
    this.add = this.add.bind(this);
  }
  add() {
    const passageId = db.add(this.state.passage);
    this.context.router.history.push("/passages/" + passageId);
  }
  render() {
    return (
      <div>
        <AppBar
          title="Memorizer"
          iconElementLeft={
            <IconButton containerElement={<Link to="/" />}><ArrowBack /></IconButton>
          }
          iconElementRight={<FlatButton label="Save" onClick={this.add} />}
        />
        <div id="passage-container">
          <TextField
            hintText="Passage"
            multiLine
            fullWidth
            value={this.state.passage}
            onChange={event => this.setState({ passage: event.target.value })}
          />
        </div>
      </div>
    );
  }
}

Add.contextTypes = {
  router: PropTypes.object,
};

export default Add;
