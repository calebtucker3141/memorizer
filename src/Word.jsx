import React, { Component } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import stringLength from 'string-length';

class Word extends Component {

  constructor(props) {
    super(props);
    this.handleFlip = this.handleFlip.bind(this);
  }

  handleFlip() {
    this.props.onFlip(this.props.item);
  }

  render() {
    if (this.props.reveal || this.props.item.show) {
      return (
        <span>
          {this.props.item.punctuation ? '' : ' '}
          <span
            role="button"
            className={classNames({ revealed: !this.props.item.show && this.props.reveal })}
            onClick={this.handleFlip}
            tabIndex={0}
          >
            {this.props.item.value}
          </span>
        </span>
      );
    }

    return (
      <span>
        {this.props.item.punctuation ? '' : ' '}
        <span
          role="button"
          onClick={this.handleFlip}
          tabIndex={0}
        >
          {'_'.repeat(stringLength(this.props.item.value))}
        </span>
      </span>
    );
  }
}

Word.propTypes = {
  item: PropTypes.shape({
    punctuation: PropTypes.bool.isRequired,
    value: PropTypes.string.isRequired,
    show: PropTypes.bool.isRequired,
  }).isRequired,
  reveal: PropTypes.bool.isRequired,
  onFlip: PropTypes.func.isRequired,
};

export default Word;
